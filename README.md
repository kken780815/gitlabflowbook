# GitLab

* GitLab是一個被廣泛使用的版控管理工具。
* GitLab可以設定開發團隊並依照身分設定階級權限，保障程式碼安全性。
* GitLab免費版即可以設定私有或公開，防程式碼外流。
* GitLab同時可以設定issue連動分支以及合併，方便管控專案開發項目，以及追蹤完成狀態。
* GitLab還能設定CI/CD使檢測部署一條龍。

# Git WorkFlow

Git為最普遍且易於使用的版本管理工具，團隊目前使用的是GitLab Flow的工作流程，這邊也大概說明其他流程的優缺點，其中依照工作流程的不同，分為三種WorkFlow:

* GitFLow
* GitHubFlow
* GitLabFlow

### Git Flow

Git Flow，是最早出現且被廣泛使用的工作流程，流程大概是這樣

![alt](image/GitFLow.png)

從圖中可發現，由於分工精細，導致在實際運用上太過於複雜，且必須頻繁切換分支。

### GitHub Flow

GitHub Flow是GitFlow的簡化版，適用於持續開發的專案項目。

![alt](image/GitHubFlow.png)

從圖中可知由於只有一條主線，使用上簡單明瞭，但是也是因此所以不適合多版本的維護。

### GitLab Flow

GitLab Flow是集合了Git Flow 以及 GitHub Flow的優點，可以有多開發版本環境，也擁有單一主線的特性，其中GitLab Flow針對產品的不同，發布方式分為持續發布和版本發佈兩種方式，但不碖哪種方式，GitLab Flow都遵守上游優先（upsteam first）的原則。

##### 上游優先 Upstream First

這邊指的就是只存在一個主分支master，其他分支都是他的下游，在開發中，所有新的需求都必須從主分支建立分支，待修正完後向管理者發起MR(Merge request)，進行Code Review討論，確認沒問題後，再由管理者Merge進主分支master，ㄧ切都由主分支開始，這邊與GitHub Flow相同，然而在發佈版本中，GitLab Flow會針對發布版本建立發佈 Production 分支，甚至針對不同環境建立不同分支，這邊就與GitHub Flow不管開發或者發布都只有一條主線，有所不同。

##### 持續發布

在持續發布中，會建議多建立一個分支為預發分支pre-production，依照實際開發狀況可以照開發環境建立不同分支，例如開發環境、測試環境、發布環境等等，下圖為master、pre-production、production：

![alt](image/GitLab-Flow1.png)

依上圖來說，開發都在master分支上，完成後發布至pre-production，master可以繼續做開發，pre-production也測完穩定之後，再發布到production環境中。

##### 版本發佈

在版本發佈中，開發結束後要發佈版本時，從主線拉出版本分支。

![alt](image/GitLabFlow2.png)

如圖從master分支建立 2-3-Stable 分支，當2-3-Stable，要更新2.3.1版本時，則從master用git cherry-pick 挑選需要的 commit 到 2-3-Stable 分支上，2-4-Stable版本也是如此，這樣可以同時維護多個版本，卻又不會相影響。
