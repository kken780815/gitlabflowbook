# issue處理

1. 檢視To-do List。![alt](../image/dev_ToDoList.png)
2. 點選要處理的issue，點選建立分支以及發起合併請求草稿。![alt](../image/dev_createMR.png)
3. 到Boards把issue從待處理移動到處理中標籤。![alt](../image/dev_issue_board2.png)
4. 確認問題單有無需要異動資料庫，如需異動，則向資料庫管理人員申請異動。
5. 處理完成後，先在本地切換至main主線pull最新版本確保本地的main為最新版，接著切換到issue分支進行**rebase**，確認與main有無衝突。
6. 程式碼確認無衝突後，推送至遠端issue分支。
7. 選擇剛剛建立的MR(Merge Request)，點選make as ready表示已經處理完成等待合併。![alt](../image/dev_pushToRemote3.png)
