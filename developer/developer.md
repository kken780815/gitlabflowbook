# 開發人員規範

#### 注意事項

    - 每天檢視To-Do List，確認工作事項。
    - Commit的資訊確實填寫。
    - 確保main主線，與遠端一致。
    - 推上遠端之前，記得要reabase。

其他事項處理流程:

* [issue處理流程](dev_issue.md)
