# 建立問題單/功能

在Gitlab功能與問題單統一用issue建立，依照類型的不同，命名規則如下

* 問題單 - `bug-描述`
* 功能 - `feature-描述`
* 緊急修復 - `hotfix-描述`

下面是建立issue的流程

1. 建立問題單![alt](../image/man_issueCreate.png)
2. 填寫標題![alt](../image/man_issueTitle.png)
3. 輸入說明內容![alt](../image/man_issueDescri.png)
4. 選擇人員或者不指派![alt](../image/man_issueAssignees.png)
5. 選擇所屬里程碑![alt](../image/man_issueMilestone.png)
