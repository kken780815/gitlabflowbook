# MileStone的建立

1. 依照專案的發布日或者版本號建立里程碑。![alt](../image/new_milestone.png)
2. 依照功能需求建立開發的issue項目。![alt](../image/connet_mileStone.png)
3. 未來可以依照完成項目來檢視該里程碑的進度。![alt](../image/milestone.png)
