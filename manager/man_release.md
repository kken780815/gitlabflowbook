# 版本發布流程

確認main版本是否無異狀，優先從main開發環境，併入re-Product測試環境，等待功能測試無誤，最後再部署至product正式環境，並檢視CI/CD狀態是否成功。
