# 管理人員規範

#### 注意事項

    - 每天檢視MergeRequest List處理合併。
    - 確保合併來源分支以及目標分支。
    - 每次推送要檢視資料異動是否正確。
    - 合併完成後記得檢視CI/CD結果是否有異常。
    - 分支記得清除避免雜亂。

其他事項處理流程:

* [里程碑建立](man_milestone.md)
* [Merge合併流程](man_merge.md)
* [發布版本流程](man_release.md)
* [建立問題單/功能](man_issueCreate.md)
