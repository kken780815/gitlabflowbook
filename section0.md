# 部門GitLabFlow開發規範

這邊我們採用的管理流程主要依照GitLabFlow再依照需求去做延伸，此章節主要介紹

* [分支](#分支)
* [Label](#label)
* [MileStone](#milestone)
* [CI/CD](#cicd)
* [版更時間](#版更時間)
* [命名規則](#命名規則)

### 分支

其分支主要分為

* main
* localTest
* AzureTest
* AzureProduct

![alt text](image/branch.png)

##### main

主要負責開功能開發，所有程式碼異動都會在這裡。

##### localTest

主要是功能開發完成，準備進行測試的本地測試區，這邊版本都會在最新版。

##### AzureTest

Azure功能的測試區。

##### AzureProduct

Azure正式上架的發布區。

### Label

Label可以方便管理人員區分問題單狀態，以了解專案目前進度，每個人員會有自己的所屬Label，專案開那中也會有流程Label，這邊依照流程主要分類為

* Develop Board(開發)
  * 待處理
  * 處理中
  * 待確認
* WorkFlow Board(工作流程)
  * 待處理
  * 處理中
  * 待確認
  * 內部測試
  * 外部測試
  * 待發布
* Assignee Board(所屬人員)

### MileStone

在專案開發中，我們會訂定各個開發階段或者版本以及發布日期，因此為了方便追蹤各個階段的進度，我們可以建立MileStone(里程碑)，用於追蹤開發進度，例如08/15的Release版本需要製作3個功能，我們就可以建立0815_Release，接著再建立3個開發issue，並把issue的MileStone歸屬於0815_Release，方便管理人追蹤。

![alt](image/milestone.png)

### CI/CD

![alt](image/CICD.png)

圖片為GitLab的CI/CD流程圖，每個分支提交都需要經過CI測試，直到通過了沒問題，才可併入main再進行最終測試和部署。

### WebHook

透過WebHook推播版控訊息。

* Teams -  Teams-RD-台中裡面的雲平台GitLab頻道
* LineBot - 雲平台Line群組

### 版更時間

* **日常更新**
每天5點管理人員會依照已經完成的issue併入main去做測試驗證，測試通過後，再到AzureTest給企劃做測試，最終再到AzureProduct再去做部署。

* **發布週期**
每週三中午12點發布版本。

### 命名規則

* 問題單 - `bug-單號-描述`
* 功能 - `feature-編號-描述`
* 緊急修復 - `hotfix-描述`
